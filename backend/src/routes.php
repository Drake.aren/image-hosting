<?php

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

use Imagine\Image\ImageInterface;

$container = $app->getContainer();
$container['upload_directory'] = __DIR__ . '/../public/uploads';
$container['image_directory'] = __DIR__ . '/../public/images';
$container['image_minimal_size'] = 600;

$app->get('/', function (Request $request, Response $response, array $args) {
  $this->logger->info("route '/'");
  return $this->renderer->render($response, 'index.html', $args);
});

$app->get('/image/process', function (Request $request, Response $response, array $args) {
  $this->logger->info("route '/image/process'");
  foreach (scandir(get_absolute_url($this->get('upload_directory'))) as $image_name) {
    if ($image_name != '.gitkeep' && $image_name != '.' && $image_name != '..') {
      try {
        $imagine = new Imagine\Imagick\Imagine();
        $image_file_path = get_absolute_url($this->get('upload_directory') . DIRECTORY_SEPARATOR . $image_name);
        $this->logger->info($image_file_path);
        $image = $imagine->open($image_file_path);
        $size = $image->getSize();
        $factor = min(
          $this->get('image_minimal_size') / $size->getWidth(), 
          $this->get('image_minimal_size') / $size->getHeight()
        );
        $image->save(
          get_absolute_url($this->get('image_directory') . DIRECTORY_SEPARATOR . $image_name),
          array(
            'resolution-x' => $size->getWidth() / $factor,
            'resolution-y' => $size->getHeight() / $factor,
            'jpeg_quality' => 75
          )
        );
        unlink($image_file_path);
      } catch (Imagine\Exception\Exception $e) {
        $this->logger->info($e);
      }
    }
  }	
  $this->logger->info('Images on server process success!');  
  $response->write('Images on server process success!');
});

$app->post('/image/upload', function (Request $request, Response $response, array $args) {
  $this->logger->info("route: '/image/upload'");
  foreach ($request->getUploadedFiles() as $file) {
    if ($file->getError() === UPLOAD_ERR_OK) {
      $filename = moveUploadedFile(
        $this->get('upload_directory'), 
        $file
      );
      $this->logger->info('File ' . $filename . ' uploaded success!');
      $result_response = $response->write(
        DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $filename
      );  
      $response->withStatus(200);
    } else {
      $this->logger->info("Upload file with error: " . $file->getError());
      $response->write("Upload file with error: " . $file->getError());
      return $response->withStatus(500);
    }
  }
});

// Functions

function get_absolute_url($path) {
  $arr1 = explode('/', $path);
  $arr2 = array();
  foreach($arr1 as $seg) {
    switch($seg) {
    case '.':
      break;
    case '..':
      array_pop($arr2);
      break;
    default:
      $arr2[] = $seg;
    }
  }
  return implode('/', $arr2);
}

function moveUploadedFile($directory, UploadedFile $uploadedFile) {
  $filename = $uploadedFile->getClientFilename();
  $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
  return $filename;
}