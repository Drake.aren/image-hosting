import axios from 'axios';

const actions = {
  UploadImage(_, image) {
    let data = new FormData();
    data.append('image', image);
    return axios.post('/image/upload', data)
  },
  ProcessImage(_) {
    return axios.get('/image/process')
  },
  DownloadImageList(_) {
    axios.get('/image/list')
      .then(res => {
        return res.data
      })
      .catch(err => {
        console.log(err)
        return null
      })
  }
}

export default {
  actions
}
