import Vue from 'vue'
import Vuex from 'vuex'

import snackbar from './snackbar'
import dialog from './dialog'
import loader from './loader'
import api from './api'

Vue.use(Vuex)

const state = {
  imageList: []
}

const mutations = {
  addImageToImageList(state, item) {
    if (item != null) {
      state.imageList.push(item);
    }
  }
}

export default new Vuex.Store({
  state,
  mutations,
  modules: {
    api,
    snackbar,
    dialog,
    loader
  }
})
