const state = {
  element: null
}

const mutations = {
  setSnackbarElement(state, element) {
    state.element = element
  },
  showSnackbar(state, message) {
    state.element.MaterialSnackbar.showSnackbar({
      message: message
    })
  }
}

export default {
  state,
  mutations,
}
