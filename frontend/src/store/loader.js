const state = {
  loaderElement: null
}

const mutations = {
  setLoaderElement(state, element) {
    state.element = element
  },
  setLoaderProgress(state, procent) {
    state.loaderElement.MaterialProgress.setProgress(procent);
  },
}

export default {
  state,
  mutations,
}
