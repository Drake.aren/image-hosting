import axios from 'axios';

const state = {}

const mutations = {
  startDialog(state, element) {
    if (!element.showModal) {
      dialogPolyfill.registerDialog(dialog);
    }
    element.showModal();
  },
  closeDialog(state, element) {
    element.close();
  }
}

const actions = {
  uploadImage(state, image) {
    return axios.post('/image/upload', {
      image
    })
  }
}

export default {
  state,
  mutations,
  actions,
}
